package chinchon.game;

import java.util.LinkedList;
import java.util.Random;

public class Jugador {
	String nombre;
	int puntos;
	int turno;
	Partida part;
	LinkedList<Carta> cartas;	
	
	public Jugador(String n, int turno){
		this.nombre = n;		
		this.puntos = 0;	
		this.turno = turno;
		this.cartas = new LinkedList<Carta>();									
	}
	
	public String nombre(){
		return this.nombre;
	}
	
	public int puntos(){
		return this.puntos;
	}
	
	public LinkedList<Carta> cartas(){
		return this.cartas;
	}
	
	public boolean equals(Object o){
		return this.nombre.equals( ((Jugador) o).nombre);
	}
	
	public void cogerCarta(){
		Carta c = this.part.darCarta();
		System.out.println("El jugador "+this.nombre+" ha cogido "+c.valor()+" de "+c.palo());
		this.cartas.add(c);
	}
	
	public void cogerCartaBocaArriba(){
		Carta c = this.part.cogerBocaArriba();
		System.out.println("El jugador "+this.nombre+" ha cogido "+c.valor()+" de "+c.palo());
		this.cartas.add(c);
	}
	
	public Carta seleccionarCarta(int c){
		Carta car = this.cartas.get(c);
		System.out.println("Seleccionando carta en posicion "+c);
		return car;
		
	}
	
	public void soltarCarta(int p){

		if(this.cartas!=null){
			Carta c = this.cartas.get(p-1);
			System.out.println("El jugador "+this.nombre+" ha soltado "+c.valor()+" de "+c.palo());
			this.cartas.remove(p-1);
			this.part.soltarCarta(c);
		}
	}		
}