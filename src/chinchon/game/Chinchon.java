package chinchon.game;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class Chinchon extends Activity {
    String idioma;
    Bundle bundle;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.inicio);
        
        bundle = getIntent().getExtras();
        this.idioma=bundle.getString("Idioma");
        
        Button botonJ = (Button) findViewById(R.id.empezarJugar);

        botonJ.setOnClickListener(new View.OnClickListener(){
        	public void onClick(View view){
        		Intent intent = new Intent(Chinchon.this, MenuCrear.class);
        		
        		intent.putExtras(bundle);
        		startActivity(intent);
        		finish();
        	}
        });
        
        Button botonI = (Button) findViewById(R.id.instrucciones);
        botonI.setOnClickListener(new View.OnClickListener(){
        	public void onClick(View view){
        	    Intent intent = new Intent(Chinchon.this, Instrucciones.class);
                
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
        	}
        });
        Button botonId = (Button) findViewById(R.id.cambIdioma);
        botonId.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent = new Intent(Chinchon.this, Idioma.class);
                
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
        if (idioma.equalsIgnoreCase("Ingles")){
            botonJ.setText("Play");
            botonI.setText("Instructions");
            botonId.setText("Language");
        }
    }
    
    public void onBackPressed(){
        
		finish();
    }
}