package chinchon.game;

import java.util.LinkedList;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import chinchon.game.Carta.Valor;

public class JServidor extends Activity{
	Thread fst;
	
	String nombre;
	Partida part;
	
	int id,primero,puntos;
	
	//Carta arriba;
	Carta carta8;
    boolean HayArriba;
    boolean robadaMonton=false;
    boolean robadaTablero=false;
    boolean seHaCogido=false;
    ImageView car1,car2,car3,car4,car5,car6,car7,car8;
    ImageView soltar,coger;
    TextView nJ1, puntosJ1, nJ2, puntosJ2, nJ3, puntosJ3, msj;
    boolean click;
	
    
	JCliente jugadorActual;
	int n = 0,cont;
	boolean turnoCorrecto;
	String idioma;
	
	Handler handler;
	String nomJug;
	boolean completo,jugMalo,anyadido,esta,finTurno,finRonda,fin,cambioTurno,menosDiez;
	Bundle bundle;
	
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);                       
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        bundle = getIntent().getExtras();  
        this.HayArriba=true;
		
		this.handler=new Handler();
		this.nombre=bundle.getString("NombreJugador");
		this.idioma=bundle.getString("Idioma");
		
		this.part = new Partida(bundle.getString("NombrePartida"));
		
		this.part.jugadores.add(new JCliente(this.nombre, 1, this.part));
		
		this.fst = new Thread(new ServerThread());
				
		this.completo = false;
		this.fin = false;
		this.cambioTurno = false;
        this.click = false;
        
        part.numeroJug = Integer.parseInt(bundle.getString("Jugadores"));
        setContentView(R.layout.partidacreada); //Mostramos la pantalla inicial
        
        this.car1=(ImageView)findViewById(R.id.carta1);
        this.car2=(ImageView)findViewById(R.id.carta2);
        this.car3=(ImageView)findViewById(R.id.carta3);
        this.car4=(ImageView)findViewById(R.id.carta4);
        this.car5=(ImageView)findViewById(R.id.carta5);
        this.car6=(ImageView)findViewById(R.id.carta6);
        this.car7=(ImageView)findViewById(R.id.carta7);
        this.car8=(ImageView)findViewById(R.id.carta8);
        this.soltar=(ImageView)findViewById(R.id.cartaSoltar);
        this.coger=(ImageView)findViewById(R.id.cartaCoger);
        final TextView nP = (TextView)findViewById(R.id.nomp);
        TextView nJ=(TextView)findViewById(R.id.jug1);
        TextView puntosJ=(TextView)findViewById(R.id.puntJug1);
        msj = (TextView)findViewById(R.id.mensajes);
        
        
        msj.setText("Esperando jugadores...");
        nJ.setText(this.nombre);
        nP.setText(this.part.nombre.toString());
        puntosJ.setText(""+this.puntos);
        
        
        colocarCartas();                        //Colocamos las cartas en la pantalla
       
        this.fst.start();  
        
        coger.setOnLongClickListener(new View.OnLongClickListener(){		
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if(!robadaTablero&&!seHaCogido&&(cont==0)){
					if (part.baraja.size()>0){		    						    		   
						   carta8 = part.darCarta();
						   colocarImagen(car8,carta8);			    		   
			    		   robadaMonton=true;
			    		   seHaCogido=true;
			    		}
				}
				return false;
			}
		});
        
       	soltar.setOnLongClickListener(new View.OnLongClickListener(){		
				@Override
				public boolean onLongClick(View v) {
					if(!robadaMonton&&HayArriba&&!seHaCogido&&(cont==0)){
						colocarImagen(car8,part.soltadas.getLast());		
						carta8=part.soltadas.getLast();
			    		soltar.setImageResource(R.drawable.cartab);
			    		robadaTablero=true;
			    		HayArriba=false;
			    		seHaCogido=true;
			    		handler.post(new Runnable(){
			    			public void run(){
			    				msj.setText("Has cogido el: "+carta8.valor()+" de "+carta8.palo());
			    			}
			    		});
			    		
					}
					 return false;					
				}
		});
       	car8.setOnLongClickListener(new View.OnLongClickListener(){		
			@Override
			public boolean onLongClick(View v) {
				if(seHaCogido&&(cont==0)){
					colocarImagen(soltar,carta8);
					
			        car8.setImageResource(R.drawable.cartab);
			        //part.bArriba = carta8;
			        part.soltadas.add(carta8);
			        carta8=null;
			        robadaMonton=false;
			        robadaTablero=false;
			        HayArriba=true;
			        seHaCogido=false;
					cont = (cont+1)%part.numeroJug;
			        handler.post(new Runnable(){
			        	public void run(){
			        		msj.setText("Has soltado una carta!");
			        	}
			        });
			        cambioTurno = true;
			        finTurno = true;
				}
				//notifyAll();
				return false;
			}
       	});
       	car1.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car1,0);}}});
       	
       	car2.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car2,1);}}});
       	
       	car3.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car3,2);}}});
       	
       	car4.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car4,3);}}});
       	
       	car5.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car5,4);}}});
       	
       	car6.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car6,5);}}});
       	
       	car7.setOnClickListener(new View.OnClickListener(){
       		public void onClick(View v) {
				if(seHaCogido&&(cont==0)&&completo){colocarAlClick(car7,6);}}});
       	
       	
    }// Fin OnCreate
	
    
    
	class ServerThread implements Runnable{    	     	   
		
		public void run() {  
			
			for(int i = 2; i <= part.numeroJug; i++){
				String nAux = new String();
				nAux = "Jugador"+i;
				part.jugadores.add(new JCliente(nAux, i, part));

			}
			
			pintarJugadores();

			handler.post(new Runnable(){

				public void run(){

					msj.setText("Empieza la partida!");
				}
			});
		
			//Jugadores creados, comienza la partida
			
			menosDiez = false;
			finRonda = false;
			cambioTurno = false;
			finTurno = false;
			
			Random rnd = new Random(); 
			cont = (rnd.nextInt()%part.numeroJug);  //Generamos un num aleatorio para seleccionar el turno al azar
			if(cont < 0){
				cont = -cont;
			}
			primero = cont;
			
			while(!fin){
				while(!finRonda){    					  								
					while ( !cambioTurno ) {						
						
						jugadorActual = part.jugadores.get(cont);

						finTurno = false;
						if(cont==0){
							handler.post(new Runnable(){
								public void run(){
									msj.setText("Es tu turno!");
								}
							});
							while(!finTurno){
								
							}
						}else{
							jugadorActual.realizarJugada();
							if(!finRonda){
								cont = (cont+1)%part.numeroJug;
							}						
							cambioTurno = true;
						}																																
					} // fin de instruccion while turno
					
					//cambioTurno = false;
					jugadorActual = part.jugadores.get(cont);
					handler.post(new Runnable(){
						public void run(){
							msj.setText("Es el turno de: "+jugadorActual.nombre);
						}
					});	
				} //fin while Ronda
				//actualizarPuntos();
				
			} //fin while partida
			handler.post(new Runnable(){
				public void run(){
					if(idioma.equals("Ingles")){
						mostrarContIngles();
					}else{
						mostrarContEspanyol();
					}
				}
			});	
			
		}
			
		
	    
		 public void mostrarContEspanyol(){
		        AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
		        adb.setTitle("Fin de la partida");
	            adb.setMessage("Desea jugar otra partida?");
	            adb.setCancelable(false);
		        adb.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
		        	public void onClick(DialogInterface dialog, int id) {
		              Intent i=new Intent(JServidor.this,Chinchon.class);
		              i.putExtras(bundle);
		              startActivity(i);
		              fin = true;
		              inicializar();
		              finish(); 
		            }});
		        adb.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
		        	public void onClick(DialogInterface dialog, int id) {               
		        		dialog.cancel();
		        		finish();
		        	}});
		        adb.show();
		 }
		 
		 public void mostrarContIngles(){
			 AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
		     adb.setTitle("Game Over");
	         adb.setMessage("Do you want to continue?");
	         adb.setCancelable(false);
		     adb.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
		    	 public void onClick(DialogInterface dialog, int id) {
		              Intent i=new Intent(JServidor.this,Chinchon.class);
		              i.putExtras(bundle);
		              startActivity(i);
		              fin = true;
		              inicializar();
		              finish(); 
		         }});
		     adb.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
		          public void onClick(DialogInterface dialog, int id) {               
		        	  dialog.cancel();
		        	  finish();
		           }});
		     adb.show();
		 }
	    
		private void actualizarPuntos(){
			JCliente j;
			LinkedList<JCliente> nueva = new LinkedList<JCliente>();
			for(int i = 0; i<part.numeroJug;i++){
				j = part.jugadores.get(i);
				j.sumarPuntos(menosDiez, cont, i);
				nueva.add(j);
			}
			part.jugadores = new LinkedList<JCliente>();
			part.jugadores.addAll(nueva);
		}
		
		private void pintarJugadores(){
			
			int aux = 0;
			JCliente jaux;
			while(!completo){				
				if(aux==part.numeroJug){
					completo = true;
				}else{
					jaux = part.jugadores.get(aux);
					if(aux == 1){
						final String jug1 = jaux.nombre;
						final String punt1 = ""+jaux.puntos;
						handler.post(new Runnable(){
							public void run(){
								nJ1=(TextView)findViewById(R.id.jug2);
								puntosJ1=(TextView)findViewById(R.id.puntJug2);
								nJ1.setText(jug1);
								puntosJ1.setText(punt1);
							}
						});
					}else if(aux == 2){
						final String jug2 = jaux.nombre;
						final String punt2 = ""+jaux.puntos;
						handler.post(new Runnable(){
							public void run(){
								nJ2=(TextView)findViewById(R.id.jug3);
								puntosJ2=(TextView)findViewById(R.id.puntJug3);
								nJ2.setText(jug2);
								puntosJ2.setText(punt2);
							}
						});
					}else if(aux == 3){
						final String jug3 = jaux.nombre;
						final String punt3 = ""+jaux.puntos;
						handler.post(new Runnable(){
							public void run(){
								nJ3=(TextView)findViewById(R.id.jug4);
								puntosJ3=(TextView)findViewById(R.id.puntJug4);
								nJ3.setText(jug3);
								puntosJ3.setText(punt3);	
							}
						});
					}							
					aux++;
				}
				
			}
			
		}	
    	   
	}//Termina Thread

	public void inicializar(){
		//fin = false;
		part.jugadores = new LinkedList<JCliente>();
	}
	
	public boolean equals(Object o){
		return this.nombre.equals( ((Jugador) o).nombre);
	}
	
	public void cogerCarta(){
		Carta c = part.darCarta();
		part.jugadores.get(0).cartas.add(c);
	}
	
	public void cogerCartaBocaArriba(){
		Carta c = part.cogerBocaArriba();
		if(!part.soltadas.isEmpty()){
			colocarImagen(soltar, part.soltadas.getLast());
		}else{
			handler.post(new Runnable(){
				public void run(){
					soltar.setImageResource(R.drawable.cartab);
				}
			});
		}
		
		part.jugadores.get(0).cartas.add(c);
	}
	
	public Carta seleccionarCarta(int c){
		Carta car = part.jugadores.get(0).cartas.get(c);
		
		return car;
		
	}
	
	public void soltarCarta(int p){
		JCliente jug = part.jugadores.get(0);
		if(jug.cartas!=null){
			Carta c = jug.cartas.get(p-1);
			jug.cartas.remove(p-1);
			this.part.soltarCarta(c);
		}
	} 
    
    public void colocarAlClick(ImageView vista,int elem){
    	LinkedList<Carta> aux = part.jugadores.get(0).cartas;
    	Carta carta=aux.get(elem);
 	    aux.set(elem, carta8);
 
 	    colocarImagen(vista,carta8);
 	    carta8=carta;
 	    colocarImagen(car8,carta);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        if(this.idioma.equalsIgnoreCase("Ingl�s")){           
            menu.add(0,1,0,"Close the round");
            menu.add(0, 2, 0, "Exit");
        }else{           
            menu.add(0,1,0, "Cerrar");
            menu.add(0, 2, 0, "Salir"); 
        }      
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case 1:
        	boolean valeC;
        	valeC=comprobarCerrar();
        	
        	   if(valeC){
        		   mostrarCerrar();
        	   }else{
        		   mostrarNoCerrar();
        	   }
        	
        	break;
        case 2:
            if(this.idioma.equalsIgnoreCase("Ingles")){
                mostrarAlertaIngles();
            }else{
                mostrarAlertaEspanyol();
            }

            break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public boolean comprobarCerrar(){
    	
    	boolean res=false;
    	if(this.carta8.valor().equals(Valor.UNO)||this.carta8.valor().equals(Valor.DOS)||this.carta8.valor().equals(Valor.TRES)){
    		res=true;
    	}
    	return res;
    }
    
    public void colocarCartas(){
        
        colocarImagen(car1,this.part.jugadores.get(0).cartas.get(0));
     
        colocarImagen(car2,this.part.jugadores.get(0).cartas.get(1));
        
        colocarImagen(car3,this.part.jugadores.get(0).cartas.get(2));
        
        colocarImagen(car4,this.part.jugadores.get(0).cartas.get(3));

        colocarImagen(car5,this.part.jugadores.get(0).cartas.get(4));
        
        colocarImagen(car6,this.part.jugadores.get(0).cartas.get(5));
        
        colocarImagen(car7,this.part.jugadores.get(0).cartas.get(6));
        
        colocarImagen(soltar,this.part.soltadas.getLast());
        
    }    
 
    public void cambiarImagenAlClick(ImageView vista,int elem){
    	
    	Carta carta=this.part.jugadores.get(0).cartas.get(elem);
    	this.part.jugadores.get(0).cartas.add(elem, this.carta8);
    
    	colocarImagen(vista,this.carta8);
    	this.carta8=carta;
    	ImageView car8=(ImageView)findViewById(R.id.carta8);
    	colocarImagen(car8,carta);
    	
    }
    
    /*Cogemos el recurso que se corresponde al nombre de la imagen y la mostramos*/
    public void mostrarImagen(ImageView im,String nombre) {
        String uri = "drawable/"+nombre;      
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());       
        Drawable image = getResources().getDrawable(imageResource);
        im.setImageDrawable(image);
    }
    
    public void colocarImagen(ImageView imagenA,Carta cartaA){
    	if (cartaA!=null){
    	final ImageView imagen=imagenA;
    	final Carta carta=cartaA;
        handler.post(new Runnable(){
    	   public void run(){
    	
    		   mostrarImagen(imagen,carta.valor().toString().toLowerCase()+carta.palo().toString().toLowerCase());
    	   }
       }); 
    	}
    }
    public void onBackPressed(){
    	if(this.idioma.equalsIgnoreCase("Ingles")){
    	    mostrarAlertaIngles();
    	}else{
    	    mostrarAlertaEspanyol();
    	}
    }
    public void mostrarAlertaEspanyol(){
        AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
        adb.setTitle("Salir");
        adb.setMessage("Esta seguro de que desea salir de la partida?");
        adb.setCancelable(false);
        adb.setPositiveButton("Si", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
              Intent i=new Intent(JServidor.this,Chinchon.class);
              i.putExtras(bundle);
              startActivity(i);
              fin = true;
              inicializar();
              fst.stop();
              finish(); 
           }});
        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {               
            dialog.cancel();
           }});
        adb.show();
    }
    public void mostrarAlertaIngles(){
        AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
        adb.setTitle("Exit");
        adb.setMessage("Do you want go back to the main menu?");
        adb.setCancelable(false);
        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
              Intent i=new Intent(JServidor.this,Chinchon.class);
              i.putExtras(bundle);
              startActivity(i);
              fin = true;
              inicializar();
              fst.stop();
              finish(); 
           }});
        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
        	  public void onClick(DialogInterface dialog, int id) {               
        		  dialog.cancel();
        	  }});
        adb.show();
    }
    public void mostrarCerrar(){
    	/*AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
    	if(this.idioma.equalsIgnoreCase("Ingl�s")){
    		adb.setTitle("Error");
            adb.setMessage("You can close the round. You should have, at least, a straight,a tip or a Chinchon");
    	}else{
    		adb.setTitle("Error");
            adb.setMessage("No puedes cerrar la partida. Debes tener, al menos, una escalera, un tr�o/cuarteto o un Chinchon");
    	}
        
        adb.setCancelable(false);
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
              
              dialog.cancel(); 
           }});
        adb.show();*/
    	finTurno = true;
    	cambioTurno = true;
    	finRonda = true;
    	fin = true;
    	
    	
    }
    public void mostrarNoCerrar(){
    	AlertDialog.Builder adb=new AlertDialog.Builder(JServidor.this);
    	if(this.idioma.equalsIgnoreCase("Ingl�s")){
    		adb.setTitle("Error");
            adb.setMessage("You can close the round. The card value should be less or equal than three");
    	}else{
    		adb.setTitle("Error");
            adb.setMessage("No puedes cerrar la partida. La carta debe tener un valor menor o igual a tres");
    	}
        
        adb.setCancelable(false);
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
              
              dialog.cancel(); 
           }});
        adb.show();
    }

}
   
   