package chinchon.game;

public class Carta {

	enum Palo{OROS, COPAS, ESPADAS, BASTOS}
	enum Valor{UNO, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA, CABALLO, REY}
	
	private Palo p;
	private Valor v;
	
	
	public Carta(Palo p, Valor v){
		this.p = p;
		this.v = v;
	}
	
	public Palo palo(){
		return this.p;
	}
	
	public Valor valor(){
		return this.v;
	}
	
	public Valor sigValor(){
		Valor val = null;
		Valor v = this.valor();
		switch (v){
		case UNO:
			val = Valor.DOS;
			break;
		case DOS:
			val = Valor.TRES;
			break;
		case TRES:
			val = Valor.CUATRO;
			break;
		case CUATRO:
			val = Valor.CINCO;
			break;
		case CINCO:
			val = Valor.SEIS;
			break;
		case SEIS:
			val = Valor.SIETE;
			break;
		case SIETE:
			val = Valor.SOTA;
			break;			
		case SOTA:
			val = Valor.CABALLO;
			break;
		case CABALLO:
			val = Valor.REY;
			break;
		case REY:
			val = null;
			break;
		}
		return val;
	}
	
	public Valor antValor(){
		Valor val = null;
		Valor v = this.valor();
		switch (v){
		case UNO:
			val = null;
			break;
		case DOS:
			val = Valor.UNO;
			break;
		case TRES:
			val = Valor.DOS;
			break;
		case CUATRO:
			val = Valor.TRES;
			break;
		case CINCO:
			val = Valor.CUATRO;
			break;
		case SEIS:
			val = Valor.CINCO;
			break;
		case SIETE:
			val = Valor.SEIS;
			break;			
		case SOTA:
			val = Valor.SIETE;
			break;
		case CABALLO:
			val = Valor.SOTA;
			break;
		case REY:
			val = Valor.CABALLO;
			break;
		}
		return val;
	}
	
	public String toString(){			
		return ""+this.valor()+" de "+this.palo();
	}
}
