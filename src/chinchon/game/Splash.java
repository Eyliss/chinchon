package chinchon.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class Splash extends Activity{
   
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       requestWindowFeature(Window.FEATURE_NO_TITLE);
       setContentView(R.layout.spl);

       Thread splashThread = new Thread() {
          @Override
          public void run() {
             try {          
                  
                   sleep(4000);
             } catch (InterruptedException e) {
                 // do nothing
             } finally {
                finish();
                Intent i = new Intent();
                Bundle bun=new Bundle();
                bun.putString("Idioma", "Espanol");
                i.putExtras(bun);
                i.setClassName("chinchon.game",
                               "chinchon.game.Chinchon");
                startActivity(i);
             }
          }
       };
       splashThread.start();
    }
}
