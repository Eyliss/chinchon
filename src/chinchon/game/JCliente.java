package chinchon.game;
import java.util.LinkedList;


import chinchon.game.Carta.Palo;
import chinchon.game.Carta.Valor;




@SuppressWarnings("serial")
public class JCliente extends Jugador{
	
	boolean primera;
	
	public JCliente(String nom, int turn, Partida p){
		super(nom, turn);
		this.part = p;
		this.cartas.addAll(p.repartir());
		this.primera = true;		
	}
	
	public boolean equals(Object o){
		return this.nombre.equals(((JCliente) o).nombre);
	}
	
	public Carta realizarJugada(){
		Carta res=null;
		LinkedList<Carta> aux=new LinkedList<Carta>();
		LinkedList<Carta> trioEsc=new LinkedList<Carta>();
		
		
		aux.addAll(this.cartas);
		Carta car=this.part.cogerBocaArriba();
		aux.add(car);
		this.cartas.add(car);
			
		trioEsc=buscarTrio(aux);  //Miramos si hay trio
		while(!trioEsc.isEmpty()){
			aux.removeAll(trioEsc);
			trioEsc=buscarTrio(aux);
		}
		
		trioEsc=buscarEscalera(aux);  //Miramos si hay escalera
		while(!trioEsc.isEmpty()){
			aux.removeAll(trioEsc);
			trioEsc=buscarEscalera(aux);
		}
		
		if(aux.isEmpty()){
			//CerrarPartida
			Carta tirada=trioEsc.getFirst();
			this.part.cerrarRonda(trioEsc.getFirst());
			
		}else if(aux.size()==1){
			this.part.cerrarRonda(aux.getFirst());
				
		}else{
			res=aux.getFirst();
			this.cartas.remove(res);
		}
		return res;
		
	}
	
	public void sumarPuntos(boolean mD, int ganador, int jug){
		if(mD){
			if(ganador == jug){
				this.puntos = this.puntos -10;
			}else{
				sumarResto(this.cartas);
			}
		}else{
			LinkedList<Carta> sinLigar = new LinkedList<Carta>();
			sinLigar.addAll(separarJugadas(this.cartas));
			sumarResto(sinLigar);
		}		
	}
	
	public LinkedList<Carta> buscarTrio(LinkedList<Carta> Mano){
		LinkedList<Carta> res = new LinkedList<Carta>();
		Valor v;
		int i = 0;
		
		while(res.size()<3&&i<Mano.size()){
			res = new LinkedList<Carta>();
			v = Mano.get(i).valor();
			for(Carta c:Mano){
				if(v.equals(c.valor())){
					res.add(c);
				}
			}
			i++;
		}
		
		if(res.size()<3){
			res = new LinkedList<Carta>();
		}	
		return res;
	}
	
	public LinkedList<Carta> buscarEscalera(LinkedList<Carta> Mano){
		LinkedList<Carta> res = new LinkedList<Carta>();
		Valor ant, sig;
		Palo p;
		int i = 0;
		
		while(res.size()<3&&i<Mano.size()){
			res = new LinkedList<Carta>();
			
			ant = Mano.get(i).antValor();
			sig = Mano.get(i).sigValor();
			p = Mano.get(i).palo();
			res.add(Mano.get(i));
			p = Mano.get(i).palo();
			for(Carta c:Mano){
				if(ant != null){
					if(ant.equals(c.valor())&&p.equals(c.palo())){
						res.add(c);
						ant = c.antValor();
					}
				}
				if(sig != null){
					if(sig.equals(c.valor())&&p.equals(c.palo())){
						res.add(c);
						sig = c.sigValor();
					}
				}
			}
			i++;
		}
		
		if(res.size()<3){
			res = new LinkedList<Carta>();
		}	
		return res;
	}
	
	public LinkedList<Carta> separarJugadas(LinkedList<Carta> mano){
		boolean hayJugada = false;
		LinkedList<Carta> res = new LinkedList<Carta>();
		LinkedList<Carta> jug1, jug2;
		jug1 = new LinkedList<Carta>();
		jug2 = new LinkedList<Carta>();
		res.addAll(mano);
		jug1.addAll(buscarTrio(res));
		this.cartas.removeAll(jug1);
		if(jug1.size()!=0){
			jug2.addAll(buscarTrio(res));
			res.removeAll(jug2);
			if(jug2.size()!=0){
				hayJugada = true;
			}else{
				jug2.addAll(buscarEscalera(res));
				res.removeAll(jug2);
				if(jug2.size()!=0){
					hayJugada = true;
				}				
			}
		}else{
			jug1.addAll(buscarEscalera(res));
			res.removeAll(jug1);
			if(jug1.size()!=0){
				jug2.addAll(buscarEscalera(res));
				res.removeAll(jug2);
				if(jug2.size()!=0){
					hayJugada = true;
				}	
			}
		}
		
		if(!hayJugada){
			jug1.addAll(buscarEscalera(res));
			res.removeAll(jug1);
			if(jug1.size()!=0){
				jug2.addAll(buscarTrio(res));
				res.removeAll(jug2);
				if(jug2.size()!=0){
					hayJugada = true;
				}else{
					jug2.addAll(buscarEscalera(res));
					res.removeAll(jug2);
					if(jug2.size()!=0){
						hayJugada = true;
					}				
				}
			}else{
				jug1.addAll(buscarTrio(res));
				res.removeAll(jug1);
				if(jug1.size()!=0){
					jug2.addAll(buscarTrio(res));
					res.removeAll(jug2);
					if(jug2.size()!=0){
						hayJugada = true;
					}	
				}
			}
		}
		return res;
	}
	
	public void sumarResto(LinkedList<Carta> cartas){
		for(Carta c:cartas){
			Valor v;
			v = c.valor();
			switch (v){
			case UNO:
				this.puntos = this.puntos +1;
				break;
			case DOS:
				this.puntos = this.puntos +2;
				break;
			case TRES:
				this.puntos = this.puntos +3;
				break;
			case CUATRO:
				this.puntos = this.puntos +4;
				break;
			case CINCO:
				this.puntos = this.puntos +5;
				break;
			case SEIS:
				this.puntos = this.puntos +6;
				break;
			case SIETE:
				this.puntos = this.puntos +7;
				break;
			case SOTA:
				this.puntos = this.puntos +10;
				break;
			case CABALLO:
				this.puntos = this.puntos +10;
				break;
			case REY:
				this.puntos = this.puntos +10;
				break;
			}
		}
		this.cartas = new LinkedList<Carta>();
	}
}