package chinchon.game;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import android.util.Log;

import chinchon.game.Carta.Palo;
import chinchon.game.Carta.Valor;


public class Partida {
	String nombre;
	LinkedList<JCliente> jugadores;
	LinkedList<Carta> baraja;
	LinkedList<Carta> soltadas;
	Carta bArriba;
	int numeroJug;
	
	public Partida(String n){
		this.nombre = n;		
		this.crearBaraja();
		this.soltadas = new LinkedList<Carta>();
		this.soltadas.add(this.darCarta());
		this.jugadores = new LinkedList<JCliente>();
	}
	
	public void crearBaraja(){
		
		this.baraja=new LinkedList<Carta>();
		
		for(int i=0;i<4;i++){
			for(int j=0;j<10;j++){
				Carta c = new Carta(Palo.values()[i],Valor.values()[j]);
				this.baraja.add(c);
			}
		}
	}
	
	public Carta bocaArriba(){
		return this.soltadas.getLast();
	}
	
	public String nombre(){
		return this.nombre;
	}
	
	public void soltarCarta(Carta c){
		this.soltadas.add(c);		
	}
	
	public Carta cogerBocaArriba(){
		Carta res = this.soltadas.getLast();
		this.soltadas.removeLast();
		return res;
	}
	
	public Carta darCarta(){
		Carta res;
		int c = (int) (Math.random()*baraja.size());
		res = this.baraja.get(c);
		this.baraja.remove(c);
		Log.d("CARTA DADA", res.toString());
		return res;		
	}
	
	public LinkedList<Carta> repartir(){
		LinkedList<Carta> res = new LinkedList<Carta>();
		int c;
		int ale=baraja.size();
		
		for(int i=0; i<7; i++){
			c = (int) (Math.random()*ale);
			if(ale != 0){						
				res.add(baraja.get(c));//coge una carta al azar y la introduce en las cartas del jugador
				baraja.remove(c);				
			}
			ale=baraja.size();
		}
		return res;
	}
	
	
	public void cerrarRonda(Carta c){
		this.bArriba = null;
	}
	
}