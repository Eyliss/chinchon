package chinchon.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MenuCrear extends Activity{
	Bundle bundle = new Bundle();
	String idioma;
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Bundle bund=getIntent().getExtras();
        this.idioma=bund.getString("Idioma");
        setContentView(R.layout.crear);
        
        
        final Integer[] datos =new Integer[]{2, 3, 4};
        ArrayAdapter<Integer> adaptador =new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, datos);
        final Spinner cmbOpciones = (Spinner)findViewById(R.id.Jug);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      
        cmbOpciones.setAdapter(adaptador);
        cmbOpciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				bundle.putString("Jugadores", arg0.getAdapter().getItem(position).toString());			
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        }
        	
        );
        
        Button botonC = (Button) findViewById(R.id.crearP);
        botonC.setOnClickListener(new View.OnClickListener(){
        	public void onClick(View view){
        		
        		Intent intent = new Intent(MenuCrear.this, JServidor.class);
        		
        		TextView nombreP=(TextView)findViewById(R.id.editnombre);
        		bundle.putString("NombrePartida", nombreP.getText().toString());
        		TextView nombreC=(TextView)findViewById(R.id.editjug);
        		bundle.putString("NombreJugador", nombreC.getText().toString());
        		bundle.putString("Idioma", idioma);
          		intent.putExtras(bundle);
        		
          		startActivity(intent);
        		finish();
        	}
        });
        if(this.idioma.equalsIgnoreCase("Ingles")){
            TextView titulo=(TextView)findViewById(R.id.titulocrear);
            titulo.setText("New Game");
            TextView nomPart=(TextView)findViewById(R.id.nombretext);
            nomPart.setText("Game Name");
            TextView nomJug=(TextView)findViewById(R.id.nomjug);
            nomJug.setText("Player Name");
            TextView numJug=(TextView)findViewById(R.id.njug);
            numJug.setText("Select Players");
            botonC.setText("Create");
        }
    }
    public void onBackPressed(){
    	Intent intent = new Intent(MenuCrear.this, Chinchon.class);
    	intent.putExtras(getIntent().getExtras());
		startActivity(intent);
		finish();
    }

}
