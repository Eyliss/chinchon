package chinchon.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

public class Idioma extends Activity{
    Bundle bundle;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.selectidioma);
        bundle = getIntent().getExtras();
        
        ImageView espa=(ImageView)findViewById(R.id.espanol);
        ImageView engl=(ImageView)findViewById(R.id.ingles);
        
        espa.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
              Intent intent=new Intent(Idioma.this,Chinchon.class);
              
              bundle.putString("Idioma", "Espanol");
              intent.putExtras(bundle);
              startActivity(intent);
              finish();
                
            }
        });
        
        engl.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
              Intent intent=new Intent(Idioma.this,Chinchon.class);
    
              bundle.putString("Idioma", "Ingles");
              intent.putExtras(bundle);
              startActivity(intent);
              finish();
                
            }
        });
    }
    public void onBackPressed(){
        Intent intent=new Intent(Idioma.this,Chinchon.class);
       
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
