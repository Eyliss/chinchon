package chinchon.game;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

public class Instrucciones extends Activity{
	
	String idioma;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       requestWindowFeature(Window.FEATURE_NO_TITLE);
       setContentView(R.layout.instrucciones);
       
       Bundle bundle = getIntent().getExtras();
       this.idioma=bundle.getString("Idioma");
      
       final FrameLayout vista=(FrameLayout)findViewById(R.id.widget34);
       if(idioma.equalsIgnoreCase("Ingles")){
    	  Drawable imagen=getResources().getDrawable(R.drawable.instunoenglish) ;
    	  vista.setBackgroundDrawable(imagen);
  		
       }else{
    	   Drawable imagen=getResources().getDrawable(R.drawable.instuno) ;
     	   vista.setBackgroundDrawable(imagen);
       }
       
       vista.setOnClickListener(new View.OnClickListener() {
        
        @Override
        public void onClick(View v) {
        	if(idioma.equalsIgnoreCase("Ingles")){
        	   Drawable imagen=getResources().getDrawable(R.drawable.instdosenglish) ;
           	   vista.setBackgroundDrawable(imagen);
        		
        	}else{
        	   Drawable imagen=getResources().getDrawable(R.drawable.instdos) ;
           	   vista.setBackgroundDrawable(imagen);
        	}
           
            vista.setOnClickListener(new View.OnClickListener() {
             @Override
                public void onClick(View v) {
            	 
            	 if(idioma.equalsIgnoreCase("Ingles")){
            		 Drawable imagen=getResources().getDrawable(R.drawable.insttresenglish) ;
                 	 vista.setBackgroundDrawable(imagen);            		
            	}else{
            		Drawable imagen=getResources().getDrawable(R.drawable.insttres) ;
                	 vista.setBackgroundDrawable(imagen);             		 
            	}
            	 
                  vista.setOnClickListener(new View.OnClickListener() {
                    
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(Instrucciones.this,Chinchon.class);
                        intent.putExtras(getIntent().getExtras());
                        startActivity(intent);
                        finish();                        
                    }
                });
                }
            });
            
        }
    });
       
    }
    public void onBackPressed(){
        Intent intent=new Intent(Instrucciones.this,Chinchon.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
        finish();
    }
}
